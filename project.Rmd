---
title: "DMProject"
author: "Taxi-Man Inc."
date: "15 May 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


## Import and clean the dataset


```{r}
# soccer
soccer <- read.csv("./data/soccer_leagues.csv")
soccer$Country <- as.character(soccer$Country)
# replace "El" with "El Salvador" because of some error in the data set
soccer$Country[soccer$Country=="El"] <- "El Salvador"
soccer$Country[soccer$Country=="Bosnia"] <- "Bosnia & Herzegovina"
soccer$Country[soccer$Country=="United"] <- "United States"
soccer$Country[soccer$Country=="New"] <- "New Zealand"
soccer$Country[soccer$Country=="Trinidad"] <- "Trinidad & Tobago"
soccer$Country[soccer$Country=="Saudi"] <- "Saudi Arabia"
soccer$Country[soccer$Country=="Congo"] <- "Congo, Dem. Rep."
soccer$Country[soccer$Country=="Czech"] <- "Czech Republic"
soccer$Country[soccer$Country=="Paupa"] <- "Papua New Guinea"
soccer$Country[soccer$Country=="Puerto"] <- "Puerto Rico"
soccer$Country[soccer$Country=="Papua"] <- "Papua New Guinea"
soccer$Country[soccer$Country == "United States"] <- "United States of America"
soccer$Country[soccer$Country == "Ivory"] <- "Côte d�???TIvoire"









soccer$League <- as.character(soccer$League)
for(i in 1:nrow(soccer)) {
  if(as.character(soccer$League[i]) == "South-Korea-K-League-Classic"){
    soccer$Country[i] <- "South Korea"
  } else if(as.character(soccer$League[i]) == "South-Africa-Premier-League"){
    soccer$Country[i] <- "South Africa"
  }
}



soccer$Country <- as.factor(soccer$Country)
soccer$X <- NULL

# basketball
basketball <- read.csv("./data/nba.csv")

# country data (quite bs because only regions not individual countries)
regions <- read.csv("./data/country_facts.csv")



# a little better contry dataset
countries <- read.csv("./data/countries.csv", sep=";")


```



#### Basketball data columns

Some of the columns were not described in the nba dataset:

* HomePCT = Home.wins / Home total games
* AwayPCT = Away.wins / Away total games
* TotalPCT = Wins / Loss


#### Regions data columns

Some of the columns were not described in the regions dataset:

* Home_Away_Contrast = Home.wins / Home total games


#### Countries data columns

Some of the columns were not described in the countries dataset:

* Birthrate and Deathrate = per 1000 inhabitants/year





## Soccer data manipulation



```{r}
# Adding the Home-Away win ratios to the dataset.
soccer$HomePCT <- soccer$HomeWins/(soccer$HomeDraw+soccer$HomeLoss+soccer$HomeWins)
soccer$AwayPCT <- soccer$AwayWins/(soccer$AwayDraw+soccer$AwayLoss+soccer$AwayWins)

# Adding the total wins, total losses, and total draws columns
soccer$TotalWins <- (soccer$HomeWins + soccer$AwayWins)
soccer$TotalLoss <- (soccer$HomeLoss + soccer$AwayLoss)
soccer$totalDraws <- (soccer$HomeDraw + soccer$AwayDraw)

```


## Soccer data exploration

Here I'm assuming that draws are not wins nor losses and leave them out of these equations

```{r}

# Mean home win ratio involving the WHOLE WORLD in SOCCER:
mean(soccer$HomePCT)
# Mean away win ratio involving the WHOLE WORLD in SOCCER:
mean(soccer$AwayPCT)

# Difference probability
mean(soccer$HomePCT) / mean(soccer$AwayPCT)

```


It turns out that, in general, it is 1.68 times more likely to win in home ground rather than away in SOCCER.

## Basketball data exploration
```{r}
# Mean home win ratio involding the WHOLE WORLD in NBA:
mean(basketball$HomePCT)
# Mean away win ratio involding the WHOLE WORLD in SOCCER:
mean(basketball$AwayPCT)

# Difference probability
mean(basketball$HomePCT) / mean(basketball$AwayPCT)

```
In BASKETBALL it is 1.67 times more likely to win in home ground rather than away, which in unbelievably close to the same ratio in SOCCER.

Let's analyse the data on country-basis:

```{r}
# add separated countries
soccer_countries <- as.data.frame(unique(soccer$Country))
# fix the column name
colnames(soccer_countries) <- "country"
soccer_countries$country <- as.character(soccer_countries$country)

# assign country HomePCT values to the dataset
for(i in 1:nrow(soccer_countries)) {
    subdata <- subset(soccer, Country==soccer_countries$country[i])
    soccer_countries$HomePCT[i] <- mean(subdata$HomePCT)
}

# assign country AwayPCT values to the dataset
for(i in 1:nrow(soccer_countries)) {
    subdata <- subset(soccer, Country==soccer_countries$country[i])
    soccer_countries$AwayPCT[i] <- mean(subdata$AwayPCT)
}

soccer_countries$country <- as.factor(soccer_countries$country)
soccer_countries <- soccer_countries[order(soccer_countries$HomePCT),]



plot(soccer_countries$HomePCT,  xaxt="n", ylim = c(0,0.8), ylab = "Win probability", xlab = "Country", main = "Home and away soccer games win probabilities in different countries", pch = 16, col = "black")
points(soccer_countries$AwayPCT,pch = 16, col = "red")
axis(1, at=1:90, labels=as.character(soccer_countries$country), las = 2,   cex.axis = 0.5)
legend("topleft", pch = c(16,16),col = c("black", "red"),
       legend = c("Home games", "Away games")
       )
```


```{r}

```

```{r}
# add separated years
soccer_years <- as.data.frame(unique(soccer$Year))
# fix the column name
colnames(soccer_years) <- "Year"
soccer_years$Year <- as.numeric(soccer_years$Year)
# assign year HomePCT values to the dataset
for(i in 1:nrow(soccer_years)) {
    subdata <- subset(soccer, Year==soccer_years$Year[i])
  soccer_years$HomePCT[i] <- mean(subdata$HomePCT)
}
for(i in 1:nrow(soccer_years)) {
    subdata <- subset(soccer, Year==soccer_years$Year[i])
    soccer_years$AwayPCT[i] <- mean(subdata$AwayPCT)
}

soccer_years <- soccer_years[order(soccer_years$Year),]
plot(soccer_years$HomePCT,  xaxt="n", ylim = c(0,0.8), ylab = "Win probability", xlab = "Country", main = "Home and away soccer games win probabilities in different years", pch = 16, col = "black")
points(soccer_years$AwayPCT,pch = 16, col = "red")
axis(1, at=1:8, labels=as.character(soccer_years$Year))
legend("topleft", pch = c(16,16),col = c("black", "red"),
       legend = c("Home games", "Away games")
       )
```
If we looked at the years, the data home advantage has not changed, if we looked at all the countries.



```{r}
# add separated years
basketball_years <- as.data.frame(unique(basketball$Year))
# fix the column name
colnames(basketball_years) <- "Year"
basketball_years$Year <- as.numeric(basketball_years$Year)
# assign year HomePCT values to the dataset
for(i in 1:nrow(basketball_years)) {
    subdata <- subset(basketball, Year==basketball_years$Year[i])
  basketball_years$HomePCT[i] <- mean(subdata$HomePCT)
}
for(i in 1:nrow(basketball_years)) {
    subdata <- subset(basketball, Year==basketball_years$Year[i])
    basketball_years$AwayPCT[i] <- mean(subdata$AwayPCT)
}

basketball_years <- basketball_years[order(basketball_years$Year),]
plot(basketball_years$HomePCT,  xaxt="n", ylim = c(0,0.8), ylab = "Win probability", xlab = "Country", main = "Home and away basketball games win probabilities in different years", pch = 16, col = "black")
points(basketball_years$AwayPCT,pch = 16, col = "red")
axis(1, at=1:61, labels=as.character(basketball_years$Year), las = 2,   cex.axis = 0.6)
legend("topleft", pch = c(16,16),col = c("black", "red"),
       legend = c("Home games", "Away games")
       )

```

```{r}
# add separated countries
soccer_countries <- as.data.frame(unique(soccer$Country))
# fix the column name
colnames(soccer_countries) <- "country"
soccer_countries$country <- as.character(soccer_countries$country)
regions$Country <- as.character(regions$Country)
# remove whitespace before and after countries
regions$Country <- trimws(regions$Country)
# remove whitespace before and after countries
soccer_countries$country <- trimws(soccer_countries$country)

# remove Algeria from the data frame because there is no info about population in Algeria
for(i in 1:nrow(soccer_countries)) {
    subdata <- subset(regions, Country==soccer_countries$country[i])
    if(nrow(subdata) > 0){
      soccer_countries$population[i] <- subdata$Population
    } else {
       soccer_countries$population[i] <- NA
    }
}

for(i in 1:nrow(soccer_countries)) {
    subdata <- subset(soccer, Country==soccer_countries$country[i])
  soccer_countries$HomePCT[i] <- mean(subdata$HomePCT)
}
for(i in 1:nrow(soccer_countries)) {
    subdata <- subset(soccer, Country==soccer_countries$country[i])
    soccer_countries$AwayPCT[i] <- mean(subdata$AwayPCT)
}

soccer_countries$country <- as.factor(soccer_countries$country)
soccer_countries <- soccer_countries[order(-soccer_countries$population),]
soccer_countries <- na.omit(soccer_countries)

plot(soccer_countries$HomePCT,  xaxt="n", ylim = c(0,0.8), ylab = "Win probability", xlab = "Country", main = "Home and away soccer games 
     win probabilities in different countries ordered by country population", pch = 16, col = "black")
points(soccer_countries$AwayPCT,pch = 16, col = "red")
axis(1, at=1:86, labels=as.character(soccer_countries$country), las = 2,   cex.axis = 0.5)
legend("topright", pch = c(16,16),col = c("black", "red"),
       legend = c("Home games", "Away games")
       )

```
The plot seems to indicate that smaller countries have a smalle home advantage than bigger countries, which can probably be explained by the number of home team supporters. For example Estonia, Latvia and Lithuania are all in the bottom quarter and this can be explained by the fact that we play a lot in stadiums that are in the same cities and it is very common that many teams even have the same home stadium.




```{r echo=FALSE}

# load again the soccer data to make stuff easier
# soccer
soccer <- read.csv("./data/soccer_leagues.csv")
soccer$Country <- as.character(soccer$Country)
# replace "El" with "El Salvador" because of some error in the data set
soccer$Country[soccer$Country=="El"] <- "El Salvador"
soccer$Country[soccer$Country=="Bosnia"] <- "Bosnia & Herzegovina"
soccer$Country[soccer$Country=="United"] <- "United States"
soccer$Country[soccer$Country=="New"] <- "New Zealand"
soccer$Country[soccer$Country=="Trinidad"] <- "Trinidad & Tobago"
soccer$Country[soccer$Country=="Saudi"] <- "Saudi Arabia"
soccer$Country[soccer$Country=="Congo"] <- "Congo, Dem. Rep."
soccer$Country[soccer$Country=="Czech"] <- "Czech Republic"
soccer$Country[soccer$Country=="Paupa"] <- "Papua New Guinea"
soccer$Country[soccer$Country=="Puerto"] <- "Puerto Rico"
soccer$Country[soccer$Country=="Papua"] <- "Papua New Guinea"
soccer$Country[soccer$Country == "United States"] <- "United States of America"
soccer$Country[soccer$Country == "Ivory"] <- "Côte d�???TIvoire"

soccer$League <- as.character(soccer$League)
for(i in 1:nrow(soccer)) {
  if(as.character(soccer$League[i]) == "South-Korea-K-League-Classic"){
    soccer$Country[i] <- "South Korea"
  } else if(as.character(soccer$League[i]) == "South-Africa-Premier-League"){
    soccer$Country[i] <- "South Africa"
  }
}



soccer$Country <- as.factor(soccer$Country)
soccer$X <- NULL

# Adding the Home-Away win ratios to the dataset.
soccer$HomePCT <- soccer$HomeWins/(soccer$HomeDraw+soccer$HomeLoss+soccer$HomeWins)
soccer$AwayPCT <- soccer$AwayWins/(soccer$AwayDraw+soccer$AwayLoss+soccer$AwayWins)

# Adding the total wins, total losses, and total draws columns
soccer$TotalWins <- (soccer$HomeWins + soccer$AwayWins)
soccer$TotalLoss <- (soccer$HomeLoss + soccer$AwayLoss)
soccer$totalDraws <- (soccer$HomeDraw + soccer$AwayDraw)


soccer_countries <- as.data.frame(unique(soccer$Country))
# fix the column name
colnames(soccer_countries) <- "country"
soccer_countries$country <- as.character(soccer_countries$country)

# assign country HomePCT values to the dataset
for(i in 1:nrow(soccer_countries)) {
    subdata <- subset(soccer, Country==soccer_countries$country[i])
    soccer_countries$HomePCT[i] <- mean(subdata$HomePCT)
}

# assign country AwayPCT values to the dataset
for(i in 1:nrow(soccer_countries)) {
    subdata <- subset(soccer, Country==soccer_countries$country[i])
    soccer_countries$AwayPCT[i] <- mean(subdata$AwayPCT)
}

soccer_countries$country <- as.factor(soccer_countries$country)
soccer_countries <- soccer_countries[order(soccer_countries$HomePCT),]

# Assign Home Field Advantage(HFA) to the dataset
soccer_countries$HFA <- as.numeric(soccer_countries$HomePCT/soccer_countries$AwayPCT) - 1



```










```{r echo=FALSE}
# Make country codes uniform for later analysis

country_codes <- read.csv("./data/country-codes.csv")


soccer_countries$country <- as.character(soccer_countries$country)
country_codes$ISO3166.1.Alpha.3 <- as.character(country_codes$ISO3166.1.Alpha.3)
# assign country codes to the dataset
for(i in 1:nrow(soccer_countries)) {
    #print(soccer_countries$country[i])
    #print(country_codes$ISO3166.1.Alpha.3[country_codes$name == soccer_countries$country[i]])
    #subdata <- subset(soccer, Country==soccer_countries$country[i])
    #
    existing_name <- soccer_countries$country[i]
    compare_name <- country_codes$ISO3166.1.Alpha.3[country_codes$name == soccer_countries$country[i]]
    
    if(length(compare_name) == 0){
      soccer_countries$ISO3[i] <- NA
    }else{
      soccer_countries$ISO3[i] <- country_codes$ISO3166.1.Alpha.3[country_codes$name == soccer_countries$country[i]]
    }
}
# back to factor
soccer_countries$country <- as.factor(soccer_countries$country)
country_codes$ISO3166.1.Alpha.3 <- as.factor(country_codes$ISO3166.1.Alpha.3)


#special cases
soccer_countries$ISO3[soccer_countries$country == "Côte d�???TIvoire"] <- "CIV"
soccer_countries$ISO3[soccer_countries$country == "Congo, Dem. Rep."] <- "COD"
soccer_countries$ISO3[soccer_countries$country == "United States of America"] <- "USA"
soccer_countries$ISO3[soccer_countries$country == "Bosnia & Herzegovina"] <- "BIH"
soccer_countries$ISO3[soccer_countries$country == "England"] <- "GBR"

```





```{r}

library(rworldmap)
library(classInt)
library(RColorBrewer)

sPDF <- joinCountryData2Map(soccer_countries
,joinCode = "ISO3"
,nameJoinColumn = "ISO3"
,mapResolution = "coarse")

#getting class intervals
classInt <- classIntervals( sPDF[["AwayPCT"]]
,n=5, style = "jenks")
catMethod = classInt[["brks"]]

#getting colours
colourPalette <- brewer.pal(5,'RdPu')
#plot map
mapDevice() #create world map shaped window
mapParams <- mapCountryData(sPDF
,nameColumnToPlot="AwayPCT"
,addLegend=FALSE
,catMethod = catMethod
,colourPalette=colourPalette
,mapTitle='Probability of winning an away game, soccer')

#adding legend
do.call(addMapLegend
,c(mapParams
,legendLabels="all"
,legendWidth=0.5
,legendIntervals="data"
,legendMar = 2))
```


```{r}

str(soccer_countries)
```

```{r}

sPDF <- joinCountryData2Map(soccer_countries
,joinCode = "ISO3"
,nameJoinColumn = "ISO3"
,mapResolution = "coarse")

#getting class intervals
classInt <- classIntervals( sPDF[["HFA"]]
,n=5, style = "jenks")
catMethod = classInt[["brks"]]

#getting colours
colourPalette <- brewer.pal(5,'RdPu')
#plot map
mapDevice() #create world map shaped window
mapParams <- mapCountryData(sPDF
,nameColumnToPlot="HFA"
,addLegend=FALSE
,catMethod = catMethod
,colourPalette=colourPalette
,mapTitle='Home Field advantage heat map')

#adding legend
do.call(addMapLegend
,c(mapParams
,legendLabels="all"
,legendWidth=0.5
,legendIntervals="data"
,legendMar = 2))
```














# BASKETBALL

```{r}

# add separated countries
basketball_teams <- as.data.frame(unique(basketball$Team))
# fix the column name
colnames(basketball_teams) <- "Team"
basketball_teams$Team <- as.character(basketball_teams$Team)

# assign team HomePCT values to the dataset
for(i in 1:nrow(basketball_teams)) {
    subdata <- subset(basketball, Team==basketball_teams$Team[i])
    basketball_teams$HomePCT[i] <- mean(subdata$HomePCT)
}

# assign team AwayPCT values to the dataset
for(i in 1:nrow(basketball_teams)) {
    subdata <- subset(basketball, Team==basketball_teams$Team[i])
    basketball_teams$AwayPCT[i] <- mean(subdata$AwayPCT)
}


basketball_teams$Team <- as.factor(basketball_teams$Team)

basketball_teams <- basketball_teams[order(basketball_teams$HomePCT),]





plot(basketball_teams$HomePCT,  xaxt="n", ylim = c(0,0.8), ylab = "Win probability", xlab = "", main = "Home and away basketball games win probabilities in different teams", pch = 16, col = "black")
points(basketball_teams$AwayPCT,pch = 16, col = "red")
axis(1, at=1:56, labels=as.character(basketball_teams$Team), las = 2,   cex.axis = 0.5)
legend("topleft", pch = c(16,16),col = c("black", "red"),
       legend = c("Home games", "Away games")
       )

```




```{r}

# add separated countries
basketball_teams <- as.data.frame(unique(basketball$Team))
# fix the column name
colnames(basketball_teams) <- "Team"
basketball_teams$Team <- as.character(basketball_teams$Team)

# assign team HomePCT values to the dataset
for(i in 1:nrow(basketball_teams)) {
    subdata <- subset(basketball, Team==basketball_teams$Team[i])
    basketball_teams$HomePCT[i] <- mean(subdata$HomePCT)
}

# assign team AwayPCT values to the dataset
for(i in 1:nrow(basketball_teams)) {
    subdata <- subset(basketball, Team==basketball_teams$Team[i])
    basketball_teams$AwayPCT[i] <- mean(subdata$AwayPCT)
}


basketball_teams$Team <- as.factor(basketball_teams$Team)

basketball_teams <- basketball_teams[order(basketball_teams$AwayPCT),]





plot(basketball_teams$HomePCT,  xaxt="n", ylim = c(0,0.8), ylab = "Win probability", xlab = "", main = "Home and away basketball games win probabilities in different teams", pch = 16, col = "black")
points(basketball_teams$AwayPCT,pch = 16, col = "red")
axis(1, at=1:56, labels=as.character(basketball_teams$Team), las = 2,   cex.axis = 0.5)
legend("topleft", pch = c(16,16),col = c("black", "red"),
       legend = c("Home games", "Away games")
       )

```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```




```{r}


```
