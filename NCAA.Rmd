---
title: "NCAA"
author: "Kaur Järvpõld"
date: "May 21, 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}

#Reading in the data

NCAA_2008_2009 <- read.csv('./Data/NCAA_2008-2009.txt')
NCAA_2009_2010 <- read.csv('./Data/NCAA_2009-2010.txt')
NCAA_2010_2011 <- read.csv('./Data/NCAA_2010-2011.txt')
NCAA_2011_2012 <- read.csv('./Data/NCAA_2011-2012.txt')
NCAA_2012_2013 <- read.csv('./Data/NCAA_2012-2013.txt')
NCAA_2013_2014 <- read.csv('./Data/NCAA_2013-2014.txt')
NCAA_2014_2015 <- read.csv('./Data/NCAA_2014-2015.txt')
NCAA_2015_2016 <- read.csv('./Data/NCAA_2015-2016.txt')
NCAA_2016_2017 <- read.csv('./Data/NCAA_2016-2017.txt')

#Deleting the second row

NCAA_2008_2009 <- NCAA_2008_2009[-1,]
NCAA_2009_2010 <- NCAA_2009_2010[-1,]
NCAA_2010_2011 <- NCAA_2010_2011[-1,]
NCAA_2011_2012 <- NCAA_2011_2012[-1,]
NCAA_2012_2013 <- NCAA_2012_2013[-1,]
NCAA_2013_2014 <- NCAA_2013_2014[-1,]
NCAA_2014_2015 <- NCAA_2014_2015[-1,]
NCAA_2015_2016 <- NCAA_2015_2016[-1,]
NCAA_2016_2017 <- NCAA_2016_2017[-1,]

#Changing the column names

colnames(NCAA_2008_2009)[c(11,12,13,14)] <- c("Home_Win", "Home_Lose", "Away_Win", "Away_Lose")
colnames(NCAA_2009_2010)[c(11,12,13,14)] <- c("Home_Win", "Home_Lose", "Away_Win", "Away_Lose")
colnames(NCAA_2010_2011)[c(11,12,13,14)] <- c("Home_Win", "Home_Lose", "Away_Win", "Away_Lose")
colnames(NCAA_2011_2012)[c(11,12,13,14)] <- c("Home_Win", "Home_Lose", "Away_Win", "Away_Lose")
colnames(NCAA_2012_2013)[c(11,12,13,14)] <- c("Home_Win", "Home_Lose", "Away_Win", "Away_Lose")
colnames(NCAA_2013_2014)[c(11,12,13,14)] <- c("Home_Win", "Home_Lose", "Away_Win", "Away_Lose")
colnames(NCAA_2014_2015)[c(11,12,13,14)] <- c("Home_Win", "Home_Lose", "Away_Win", "Away_Lose")
colnames(NCAA_2015_2016)[c(11,12,13,14)] <- c("Home_Win", "Home_Lose", "Away_Win", "Away_Lose")
colnames(NCAA_2016_2017)[c(11,12,13,14)] <- c("Home_Win", "Home_Lose", "Away_Win", "Away_Lose")

#Converting necessary columns from factor to numeric

NCAA_2008_2009$Home_Win  <- as.numeric(NCAA_2008_2009$Home_Win)
NCAA_2008_2009$Home_Lose <- as.numeric(NCAA_2008_2009$Home_Lose)
NCAA_2008_2009$Away_Win  <- as.numeric(NCAA_2008_2009$Away_Win)
NCAA_2008_2009$Away_Lose <- as.numeric(NCAA_2008_2009$Away_Lose)

NCAA_2009_2010$Home_Win  <- as.numeric(NCAA_2009_2010$Home_Win)
NCAA_2009_2010$Home_Lose <- as.numeric(NCAA_2009_2010$Home_Lose)
NCAA_2009_2010$Away_Win  <- as.numeric(NCAA_2009_2010$Away_Win)
NCAA_2009_2010$Away_Lose <- as.numeric(NCAA_2009_2010$Away_Lose)

NCAA_2010_2011$Home_Win  <- as.numeric(NCAA_2010_2011$Home_Win)
NCAA_2010_2011$Home_Lose <- as.numeric(NCAA_2010_2011$Home_Lose)
NCAA_2010_2011$Away_Win  <- as.numeric(NCAA_2010_2011$Away_Win)
NCAA_2010_2011$Away_Lose <- as.numeric(NCAA_2010_2011$Away_Lose)

NCAA_2011_2012$Home_Win  <- as.numeric(NCAA_2011_2012$Home_Win)
NCAA_2011_2012$Home_Lose <- as.numeric(NCAA_2011_2012$Home_Lose)
NCAA_2011_2012$Away_Win  <- as.numeric(NCAA_2011_2012$Away_Win)
NCAA_2011_2012$Away_Lose <- as.numeric(NCAA_2011_2012$Away_Lose)

NCAA_2012_2013$Home_Win  <- as.numeric(NCAA_2012_2013$Home_Win)
NCAA_2012_2013$Home_Lose <- as.numeric(NCAA_2012_2013$Home_Lose)
NCAA_2012_2013$Away_Win  <- as.numeric(NCAA_2012_2013$Away_Win)
NCAA_2012_2013$Away_Lose <- as.numeric(NCAA_2012_2013$Away_Lose)

NCAA_2013_2014$Home_Win  <- as.numeric(NCAA_2013_2014$Home_Win)
NCAA_2013_2014$Home_Lose <- as.numeric(NCAA_2013_2014$Home_Lose)
NCAA_2013_2014$Away_Win  <- as.numeric(NCAA_2013_2014$Away_Win)
NCAA_2013_2014$Away_Lose <- as.numeric(NCAA_2013_2014$Away_Lose)

NCAA_2014_2015$Home_Win  <- as.numeric(NCAA_2014_2015$Home_Win)
NCAA_2014_2015$Home_Lose <- as.numeric(NCAA_2014_2015$Home_Lose)
NCAA_2014_2015$Away_Win  <- as.numeric(NCAA_2014_2015$Away_Win)
NCAA_2014_2015$Away_Lose <- as.numeric(NCAA_2014_2015$Away_Lose)

NCAA_2015_2016$Home_Win  <- as.numeric(NCAA_2015_2016$Home_Win)
NCAA_2015_2016$Home_Lose <- as.numeric(NCAA_2015_2016$Home_Lose)
NCAA_2015_2016$Away_Win  <- as.numeric(NCAA_2015_2016$Away_Win)
NCAA_2015_2016$Away_Lose <- as.numeric(NCAA_2015_2016$Away_Lose)

NCAA_2016_2017$Home_Win  <- as.numeric(NCAA_2016_2017$Home_Win)
NCAA_2016_2017$Home_Lose <- as.numeric(NCAA_2016_2017$Home_Lose)
NCAA_2016_2017$Away_Win  <- as.numeric(NCAA_2016_2017$Away_Win)
NCAA_2016_2017$Away_Lose <- as.numeric(NCAA_2016_2017$Away_Lose)

```

```{r}

#Adding probabilities for winning at Home & Away

NCAA_2008_2009$HomePCT <- NCAA_2008_2009$Home_Win/(NCAA_2008_2009$Home_Win+NCAA_2008_2009$Home_Lose)
NCAA_2008_2009$AwayPCT <- NCAA_2008_2009$Away_Win/(NCAA_2008_2009$Away_Win+NCAA_2008_2009$Away_Lose)

NCAA_2009_2010$HomePCT <- NCAA_2009_2010$Home_Win/(NCAA_2009_2010$Home_Win+NCAA_2009_2010$Home_Lose)
NCAA_2009_2010$AwayPCT <- NCAA_2009_2010$Away_Win/(NCAA_2009_2010$Away_Win+NCAA_2009_2010$Away_Lose)

NCAA_2010_2011$HomePCT <- NCAA_2010_2011$Home_Win/(NCAA_2010_2011$Home_Win+NCAA_2010_2011$Home_Lose)
NCAA_2010_2011$AwayPCT <- NCAA_2010_2011$Away_Win/(NCAA_2010_2011$Away_Win+NCAA_2010_2011$Away_Lose)

NCAA_2011_2012$HomePCT <- NCAA_2011_2012$Home_Win/(NCAA_2011_2012$Home_Win+NCAA_2011_2012$Home_Lose)
NCAA_2011_2012$AwayPCT <- NCAA_2011_2012$Away_Win/(NCAA_2011_2012$Away_Win+NCAA_2011_2012$Away_Lose)

NCAA_2012_2013$HomePCT <- NCAA_2012_2013$Home_Win/(NCAA_2012_2013$Home_Win+NCAA_2012_2013$Home_Lose)
NCAA_2012_2013$AwayPCT <- NCAA_2012_2013$Away_Win/(NCAA_2012_2013$Away_Win+NCAA_2012_2013$Away_Lose)

NCAA_2013_2014$HomePCT <- NCAA_2013_2014$Home_Win/(NCAA_2013_2014$Home_Win+NCAA_2013_2014$Home_Lose)
NCAA_2013_2014$AwayPCT <- NCAA_2013_2014$Away_Win/(NCAA_2013_2014$Away_Win+NCAA_2013_2014$Away_Lose)

NCAA_2014_2015$HomePCT <- NCAA_2014_2015$Home_Win/(NCAA_2014_2015$Home_Win+NCAA_2014_2015$Home_Lose)
NCAA_2014_2015$AwayPCT <- NCAA_2014_2015$Away_Win/(NCAA_2014_2015$Away_Win+NCAA_2014_2015$Away_Lose)

NCAA_2015_2016$HomePCT <- NCAA_2015_2016$Home_Win/(NCAA_2015_2016$Home_Win+NCAA_2015_2016$Home_Lose)
NCAA_2015_2016$AwayPCT <- NCAA_2015_2016$Away_Win/(NCAA_2015_2016$Away_Win+NCAA_2015_2016$Away_Lose)

NCAA_2016_2017$HomePCT <- NCAA_2016_2017$Home_Win/(NCAA_2016_2017$Home_Win+NCAA_2016_2017$Home_Lose)
NCAA_2016_2017$AwayPCT <- NCAA_2016_2017$Away_Win/(NCAA_2016_2017$Away_Win+NCAA_2016_2017$Away_Lose)

#Adding Home Field Advantage to the dataset

NCAA_2008_2009$HFA <- (NCAA_2008_2009$HomePCT/NCAA_2008_2009$AwayPCT) -1
NCAA_2009_2010$HFA <- (NCAA_2009_2010$HomePCT/NCAA_2009_2010$AwayPCT) -1
NCAA_2010_2011$HFA <- (NCAA_2010_2011$HomePCT/NCAA_2010_2011$AwayPCT) -1
NCAA_2011_2012$HFA <- (NCAA_2011_2012$HomePCT/NCAA_2011_2012$AwayPCT) -1
NCAA_2012_2013$HFA <- (NCAA_2012_2013$HomePCT/NCAA_2012_2013$AwayPCT) -1
NCAA_2013_2014$HFA <- (NCAA_2013_2014$HomePCT/NCAA_2013_2014$AwayPCT) -1
NCAA_2014_2015$HFA <- (NCAA_2014_2015$HomePCT/NCAA_2014_2015$AwayPCT) -1
NCAA_2015_2016$HFA <- (NCAA_2015_2016$HomePCT/NCAA_2015_2016$AwayPCT) -1
NCAA_2016_2017$HFA <- (NCAA_2016_2017$HomePCT/NCAA_2016_2017$AwayPCT) -1

```

```{r}

#Calculating the HFA means per year

HFA_2009 <- mean(NCAA_2008_2009$HFA)
HFA_2010 <- mean(NCAA_2009_2010$HFA)
HFA_2011 <- mean(NCAA_2010_2011$HFA)
HFA_2012 <- mean(NCAA_2011_2012$HFA)
HFA_2013 <- mean(NCAA_2012_2013$HFA)
HFA_2014 <- mean(NCAA_2013_2014$HFA)
HFA_2015 <- mean(NCAA_2014_2015$HFA)
HFA_2016 <- mean(NCAA_2015_2016$HFA)
HFA_2017 <- mean(NCAA_2016_2017$HFA)

```

```{r}

#Creating the data frame for the yearly HFA's

HFA_per_year <- data.frame(c(2009:2016))
HFA_per_year <- cbind(HFA_per_year, c(HFA_2009,HFA_2010,HFA_2011,HFA_2012,HFA_2013,HFA_2014,HFA_2015,HFA_2016))
colnames(HFA_per_year)[c(1,2)] <- c("Year", "HFA")

#Plotting the data frame

library(ggplot2)

ggplot(data=HFA_per_year, aes(x=Year, y=HFA, group=1)) + 
    geom_line(colour="#78a963", linetype="solid", size=3)
```
